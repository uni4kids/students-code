#include <Adafruit_NeoPixel.h>
#include "mega2560_pinout.h"

//variables

#define sensor0pin AP00
#define sensor1pin AP02
#define sensor2pin AP04
#define sensor3pin AP06
#define sensor4pin AP08
#define sensor5pin AP10
#define sensor6pin AP12

#define ledMatrixPin DP02

const int bRows = 6;
const int bCols = 7;
const int MCol = 8;

int board[bRows][bCols] = { 0 };  // 6 редове x 7 колони
int columnCounters[7] = { 0 };    // проверка за препълване
int testBoard01[bRows][bCols] =
{
  {0, 1, 1, 2, 2, 0, 0},
  {0, 1, 1, 2, 2, 0, 0},
  {0, 0, 1, 0, 0, 0, 0},
  {0, 0, 1, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0}
};

// vertical player 2
// row = 3, col = 3
int testBoard02[bRows][bCols] =
{
  {0, 1, 1, 2, 2, 0, 0},
  {0, 1, 1, 2, 2, 0, 0},
  {0, 0, 0, 2, 0, 0, 0},
  {0, 0, 0, 2, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0}
};

// horizontal player 1
// row = 1, col = 1
// row = 1, col = 2
// row = 1, col = 3
// row = 1, col = 4
int testBoard03[bRows][bCols] =
{
  {0, 1, 1, 2, 2, 0, 0},
  {0, 1, 1, 1, 1, 0, 0},
  {0, 0, 0, 2, 0, 0, 0},
  {0, 0, 0, 2, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0}
};

// diagonal player 1
// row = 0 col = 2 => win
// row = 1 col = 3 => win
// row = 2 col = 4 => win
// row = 3 col = 5 => win
int testBoard04[bRows][bCols] =
{
  {0, 1, 1, 2, 2, 2, 0},
  {0, 1, 1, 1, 1, 2, 0},
  {0, 0, 0, 2, 1, 1, 0},
  {0, 0, 0, 2, 0, 1, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0}
};

int testBoard05[bRows][bCols] =
{
  {0, 1, 2, 2, 1, 2, 0},
  {0, 2, 1, 1, 1, 2, 0},
  {0, 0, 1, 2, 0, 0, 0},
  {0, 1, 0, 2, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0}
};

int testBoard06[bRows][bCols] =
{
  {0, 0, 0, 0, 0, 0, 0},
  {1, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0}
};


const int thresholdLow = 30;
const int thresholdHigh = 120;
const int hollowSolidPieceBorder = 450;

unsigned long startTime = 0;
unsigned long endTime = 0;
unsigned long objectTime = 0;

bool objectDetected = false;
bool solidity = false;  // True за solid и играч 1, false за hollow и играч 2
int currentPlayer = 1;

const int ledMatrixPixelsCount = 64;
Adafruit_NeoPixel matrix(ledMatrixPixelsCount, ledMatrixPin, NEO_GRB + NEO_KHZ800);

const uint32_t red = matrix.Color(80, 0, 0);
const uint32_t green = matrix.Color(0, 80, 0);
const uint32_t white = matrix.Color(80, 80, 80);

const uint32_t player1Color = red;
const uint32_t player2Color = green;

//functions

bool checkWinner(int row, int col, int player) {
/*  if (checkVerticalWin(row, col, player, board[bRows][bCols]) || 
  checkHorizontalWin(row, col, player, board[bRows][bCols]) || 
  checkDiagonal1Win(row, col, player, board[bRows][bCols]) || 
  checkDiagonal2Win(row, col, player, board[bRows][bCols])) {
  }*/
if(checkVerticalWin(row, col, player, board[bRows][bCols])){
   return true;
}
if(checkHorizontalWin(row, col, player, board[bRows][bCols])){
   return true;
}
if(checkDiagonal1Win(row, col, player, board[bRows][bCols])){
   return true;
}
if(checkDiagonal2Win(row, col, player, board[bRows][bCols])){
   return true;
}

return false;
}
// thresholdLow = 30;
// thresholdHigh = 120;
//hollowSolidPieceBorder = 450

int analogPins[] ={sensor0pin, sensor1pin, sensor2pin, sensor3pin, sensor4pin, sensor5pin, sensor6pin};
void testAnalogPin(int analogPin){
  int value = analogRead(analogPins[analogPin]);
  Serial.print(" ");
  Serial.print(value);
}

void check(int sensorPin, int colIndex) {
  Serial.println(analogRead(sensorPin));
  int value = analogRead(sensorPin);

  if (value < thresholdLow && !objectDetected) {
    startTime = micros();
    objectDetected = true;
  }

  if (value > thresholdHigh && objectDetected) {
    endTime = micros();
    objectTime = endTime - startTime;  // timer to see which player is playing
    objectDetected = false;


    solidity = objectTime > hollowSolidPieceBorder;  // като if(solidity>...)...


    currentPlayer = solidity ? 1 : 2;  // checks if solidity is true or false and gives currentPlayer avalues based on it

    if (columnCounters[colIndex] < 6) {
      int row = 5 - columnCounters[colIndex];
      board[row][colIndex] = currentPlayer;
      columnCounters[colIndex]++;

      if (checkWinner(row, colIndex, currentPlayer)) {
        Serial.print("Player ");
        Serial.print(currentPlayer);
        Serial.println(" wins!");
      }
    } else {
      Serial.print("Column ");
      Serial.print(colIndex);
      Serial.println(" is full!");
    }
  }
}

int pixelIndexMatrix(int row, int col) {
  int delta = MCol - bCols;
  int mapCol = MCol - delta + bCols - col;
  return (row - 1) * MCol + mapCol;
}

void showMatrix(int board[bRows][bCols]) {
  for (int row = 0; row < bRows; row++) {
    for (int col = 0; col < bCols; col++) {
      uint32_t pixColor = white;
      if (board[row][col] == 1) {
        pixColor = player1Color;
      } else if (board[row][col] == 2) {
        pixColor = player2Color;
      }
      matrix.setPixelColor(pixelIndexMatrix(row, col), pixColor);
    }
  }

  matrix.show();
}
//const int bRows = 6;
//const int bCols = 7;
//const int MCol = 8;

bool validPosition(int currRow, int currCol) {
  if (currRow < 0 || currRow >= bRows || currCol < 0 || currCol >= bCols) {
    return false;
  }
  return true;
}

// Check vertical sequence of 4.
// The input player is the top checker so scan down in the board
bool checkVerticalWin(int row, int col, int player, int currboard[bRows][bCols]) {
  bool win = false;
  int count = 1;

  // ToDo: optimize to check maximum 4
  for (int i = row - 1; i >= 0; i--) {
    if (validPosition(i, col) == false) {
      break;
    }

    if (currboard[i][col] == player) {
      count++;
    } else {
      break;
    }
  }

  win = (count > 3) ? true : false;
  return win;
}

bool checkHorizontalWin(int row, int col, int player, int currboard[bRows][bCols]) {
  bool win = false;
  int count = 1;

  // First start scanning left until other than player met
  for (int i = col - 1; i >= 0; i--) {
    if (validPosition(row, i) == false) {
      break;
    }

    if (currboard[row][i] == player) {
      count++;
    } else {
      break;
    }
  }

  // Then start scanning right until other than player met
  for (int i = col + 1; i < bCols; i++) {
    if (validPosition(row, i) == false) {
      break;
    }

    if (currboard[row][i] == player) {
      count++;
    } else {
      break;
    }
  }

  win = (count > 3) ? true : false;
  return win;
}

bool checkDiagonal1Win(int row, int col, int player, int currboard[bRows][bCols]) {
  bool win = false;
  int count = 1;

  // First start scanning / until other than player met
  int delta = -1;
  for (int i = 1; i < 4; i++) {
    int currRow = row + i * delta;
    int currCol = col + i * delta;

    if (validPosition(currRow, currCol) == false) {
      break;
    }

    if (currboard[currRow][currCol] == player) {
      count++;
    } else {
      break;
    }
  }

  // Then start scanning / until other than player met
  delta = 1;
  for (int i = 1; i < 4; i++) {
    int currRow = row + i * delta;
    int currCol = col + i * delta;

    if (validPosition(currRow, currCol) == false) {
      break;
    }

    if (currboard[currRow][currCol] == player) {
      count++;
    } else {
      break;
    }
  }

  win = (count > 3) ? true : false;
  return win;
}

bool checkDiagonal2Win(int row, int col, int player, int currboard[bRows][bCols]) {
  bool win = false;
  int count = 1;

  // First start scanning \ until other than player met
  for (int i = 1; i < 4; i++) {
    int currRow = row + i;
    int currCol = col - i;

    if (validPosition(currRow, currCol) == false) {
      break;
    }

    if (currboard[currRow][currCol] == player) {
      count++;
    } else {
      break;
    }
  }

  // Then start scanning \ until other than player met
  for (int i = 1; i < 4; i++) {
    int currRow = row - i;
    int currCol = col + i;

    if (validPosition(currRow, currCol) == false) {
      break;
    }

    if (currboard[currRow][currCol] == player) {
      count++;
    } else {
      break;
    }
  }

  win = (count > 3) ? true : false;
  return win;
}

void printBoard(int currboard[bRows][bCols]) {
  for (int i = bRows - 1; i >= 0; i--) {
    for (int j = 0; j < bCols; j++) {
      Serial.print(currboard[i][j]);
      Serial.print(" ");
    }
    Serial.println();
  }
}

void setup() {
  Serial.begin(9600);
  for (int i = 0; i < bRows; i++) {
    for (int j = 0; j < bCols; j++) {
      board[i][j] = 0;
    }
  }

  matrix.begin();
  matrix.setBrightness(25);

  showMatrix(testBoard02);
  printBoard(testBoard02);
  Serial.println("test1");
  Serial.println(checkDiagonal2Win(0, 4, 1, testBoard05));
  Serial.println();

  Serial.println("test2");
  Serial.println(checkDiagonal2Win(1, 3, 1, testBoard05));
  Serial.println();

  Serial.println("test3");
  Serial.println(checkDiagonal2Win(3, 1, 1, testBoard05));
  Serial.println();

  Serial.println("test4");
  Serial.println(checkDiagonal2Win(3, 5, 1, testBoard05));
  Serial.println();
}

void loop() {
  for (int i = 0; i<7; i++){
    testAnalogPin(i);
  }
  Serial.println("");
  delay(1000);
  /* check(sensor0pin, 0);
    check(sensor1pin, 1);
    check(sensor2pin, 2);
    check(sensor3pin, 3);
    check(sensor4pin, 4);
    check(sensor5pin, 5);
    check(sensor6pin, 6);

    for (int i = 0; i < 6; i++) {
     for (int j = 0; j < 7; j++) {
       Serial.print(board[i][j]);
       Serial.print(" ");
     }
     Serial.println();
    }
    Serial.println();

    showMatrix(board);
    delay(500);*/
}
