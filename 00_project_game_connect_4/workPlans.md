# План за работа 

## 01 занятие 2024.10.05

1. Запознаване на групата с проекта

## 02 занятие 2024.10.12

1. Проверка на 3D дизайн
    - отвори за виждане на пуловете не са разположени добре във всички колони
1. Електроника
    - обсъждане на подобрение на запояване на сензорите и държачи за тях 

## 03 занятие 2024.10.19

1. Електроника
    - тестово решение: крачетата на сензора се отчупват лесно, да се тества може ли да се запои на платка и тогава да се слагат кабели?
    - резистори за свързване към сензорите, да се уточнят стойностите и да се подготви върху платка 
1. 3D дизайн
    - проектиране на нов държач за сензор
    - отпечатване на прототип за един сензор 
    - да се направи запои на един сензор + резистори + изводи за свързване към платка


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- сензори 2-3 за тест + 7бр за реално използване
- резистори 2вида x 7бр. (да се уточнят стойностите)
- 3D принтер
- филамент 

Резултат:
- дизайн на ново закрепване на сензорите, първи пробен печат. Необходима е корекция...

## 04 занятие 26.10.2024

1. Електроника
    - резистори за свързване към сензорите, да се уточнят стойностите и да се подготви върху платка 
1. 3D дизайн
    - корекция на нов държач за сензор
    - отпечатване на прототип за един сензор 
    - да се направи запои на един сензор + резистори + изводи за свързване към платка


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- сензори 2-3 за тест + 7бр за реално използване
- резистори 2вида x 7бр. (да се уточнят стойностите)
- 3D принтер
- филамент 

Резултат:  
- избрани резистори след като тествахме сензор R = 100 Ohm и R<sub>L</sub> = 2.2k Ohm 
- резултатния диапазон за обект и без обект е 150 и 910 (да се калибрира за дадена платка и да се прецени грешка +/- 30 дали е достатъчна)

## 05 занятие 09.11.2024

1. Електроника
    - резистори за свързване към сензорите, да се запоят върху платка 
1. 3D дизайн  

    ![04_board_module](./img/04_single_sensor_board.jpg)
    1. Вариант 1 кабелите на запоения сензор на single_sensor_board са на позиции колони 2 и 4 
        - корекция на нов държач за сензор
        - отпечатване на прототип за един сензор 


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- сензори 2-3 за тест + 7бр за реално използване
- резистори 2вида x 7бр. (да се уточнят стойностите)
- 3D принтер
- филамент 

Резултат:  
- избрани резистори след като тествахме сензор R = 100 Ohm и R<sub>L</sub> = 2.2k Ohm 
- резултатния диапазон за обект и без обект е 150 и 910 (да се калибрира за дадена платка и да се прецени грешка +/- 30 дали е достатъчна)

## 06 занятие 16.11.2024
1. 3D дизайн  
    - корекция на нов държач за сензор
    - отпечатване на прототип за един сензор и проверка за монтиране

    ![04_board_module](./img/04_single_sensor_board.jpg)
    1. Вариант 2а кабелите на запоения сензор на single_sensor_board са на отделни позиции колони 1 и 5
        - да се тества ако кабелите излизат от страната на сензора
    1. Вариант 2б кабелите на запоения сензор на single_sensor_board са на отделни позиции колони 1 и 5
        - да се тества ако кабелите излизат от страната противоположна на сензора

Аспекти на разсъждения:
- удобство при монтажа
- къде ще излизат кабелите 
- ако е необходимо променете съществуващия краен вариант
        
 Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- сензори 2-3 за тест + 7бр за реално използване
- режещи инструменти
    - резци малки и големи
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици   
- 3D принтер
- филамент    

## 07 занятие 23.11.2024
1. Електроника
    - резистори за свързване към сензорите, да се запоят върху платка. 
    
Да се използват рейки за лесно свързване на целите модули   
Да влизат(от модула със сензорите):
- рейка 1 - VCC(2,3) sensor
- рейка 2 - пин 1 sensor
- рейка 3 - пин 4 sensor

Да излизат(към ардуино):
- рейка 4 - ардуино In 1-7
- рейка 5 - захранване към ардуино пин VCC и GND

Когато финалния 3D дизайна за монтаж на сензорите е уточнен да се монтират сензорите и да се подготвят


7x15 board  
![02_board_module](./img/02_board_module.jpg) 

Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- резистори 2вида x 7бр. (R = 100 Ohm и R<sub>L</sub> = 2.2k Ohm )

## 08 занятие 07.12.2024
1. 3D дизайн  
    - корекция на нов държач за сензор
    - отпечатване на прототип за един сензор и проверка за монтиране

## 09 занятие 14.12.2024
1. Електроника
    - резистори за свързване към сензорите, да се запоят върху платка. 
    
Да се използват рейки за лесно свързване на целите модули   
Да влизат(от модула със сензорите):
- рейка 1 - VCC(2,3) sensor
- рейка 2 - пин 1 sensor
- рейка 3 - пин 4 sensor

Да излизат(към ардуино):
- рейка 4 - ардуино In 1-7
- рейка 5 - захранване към ардуино пин VCC и GND

Когато финалния 3D дизайна за монтаж на сензорите е уточнен да се монтират сензорите и да се подготвят


7x15 board  
![02_board_module](./img/02_board_module.jpg) 

Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- резистори 2вида x 7бр. (R = 100 Ohm и R<sub>L</sub> = 2.2k Ohm )

## 10 занятие 11.12.2024

Модел вариант Дани - държач за сензори да се удебели 

Платка държаща сензора: w:13(max) mm h:6.5(max) mm


## 11 занятие
Алгоритъм за победа на играч:
- засичане на хоризонтална линия
- засичане на вертикална линия
- засичане на диагонали
Показване на игралната дъска върху LED матрица

## 12 занятие

## XX занятие
Да се реализира тестова програма, с която да се проверяват сензорите и включените кабели към аналоговите входове:
- активира се аналогов вход от сензор да светва съответен ред на матрицата

Да се събере кода за засичане на нов пул с кода за победа.  
[Source code](src/4_in_line/4_in_line.ino)

Изработване на кутия за платката и кабелите, може да е подобна на тази от проекта камък,ножица,хартия като се модифицира да има от двете страни отвори за жиците. Влизащи от играта и излизащи към червената платка.