#include <Adafruit_NeoPixel.h>
#include <DHT.h>
Adafruit_NeoPixel rgb = Adafruit_NeoPixel (1, A2, NEO_GRB + NEO_KHZ800);
#define DHTPIN 4
#define DHTTYPE DHT22
#define LED 13
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  rgb.setBrightness(70);
  pinMode(A0, OUTPUT);
  Serial.begin(9600);
  dht.begin();
  rgb.begin();
}

//Constants



int chk;
float hum;  //Stores humidity value
float temp; //Stores temperature value

void loop()
{
  temp = dht.readTemperature();
  hum = dht.readHumidity();
  Serial.print("Humidity: ");
  Serial.print(hum);
  Serial.print(" %, Temp: ");
  Serial.print(temp);
  Serial.println(" Celsius");
  delay(1000); //Delay 2 sec.
  if (temp > 20 && hum > 69) {
    rgb.setPixelColor(0, 255, 0, 0);
    rgb.show();

  }
  else {
    digitalWrite(LED, LOW);
  }
  if (temp > 15 && temp < 20 && hum > 60 && hum < 69) {
    rgb.setPixelColor(0, 222, 235, 52);
    rgb.show();
    
  }
  else {
    digitalWrite(LED, LOW);
  }
   if (temp < 15 && hum < 60 && hum > 40) {
    rgb.setPixelColor(0, 0, 255, 0);
    rgb.show();
    
  }
  else {
    digitalWrite(LED, LOW);
  }
}
