#include <Adafruit_NeoPixel.h>
#define RGB_Matrix_pin A3
#define brightness (25)
const int rgbMatrixPixelsCount = 64;
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel (rgbMatrixPixelsCount,
                              RGB_Matrix_pin, NEO_GRB + NEO_KHZ800);
int a[rgbMatrixPixelsCount][3];

void setup() {
  rgbMatrix.begin();
  rgbMatrix.setBrightness(brightness);

  
}

void loop() {
for (int i = 0; i < rgbMatrixPixelsCount; i ++) {
    for (int m = 0; m < 3; m ++){
     a[i][m] = random(0, 256); 
    }
      
  }
  for (int i = 0; i < rgbMatrixPixelsCount; i ++) {
    rgbMatrix.setPixelColor(i, a[i][0], a[i][1], a[i][2]);
   
  }
rgbMatrix.show();
delay(2000);
}
