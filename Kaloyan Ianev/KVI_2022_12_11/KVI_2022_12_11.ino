#include "mega2560_pinout.h"
#include <TM1637Display.h>
#include <Adafruit_NeoPixel.h>

#define TEST_DELAY 500

TM1637Display disp(SEGMENT_DISPLAY_CLK_PIN, SEGMENT_DISPLAY_DIO_PIN);

const int rgbMatrixPixelsCount = 8;
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(rgbMatrixPixelsCount, RGB_LED_MATRIX_PIN, NEO_GRB + NEO_KHZ800);

// Object color definitions
uint32_t offColor = rgbMatrix.Color(0, 0, 0);
uint32_t matchColor = rgbMatrix.Color(0, 255, 0);
uint32_t missColor = rgbMatrix.Color(255, 0, 0);


#define SECOND (1000)
uint32_t currTime = 0, lastTime = 0;

uint8_t disarmCode = 0;
bool helperEnable = true;

void beep(bool on, int beepSpeed);
void codeHelper(bool enabled, int matchCode, int currCode);

void setup() {
  randomSeed(analogRead(3));
  disarmCode = random(0, 16);
  
  Serial.begin(9600);
  pinMode(DIP1_PIN, INPUT);
  pinMode(DIP2_PIN, INPUT);
  pinMode(DIP3_PIN, INPUT);
  pinMode(DIP4_PIN, INPUT);

  pinMode(BLUE_LED_PIN, OUTPUT);
  pinMode(RED_LED_PIN, OUTPUT);

  pinMode(BUZZER_PIN, OUTPUT);

  pinMode(BIG_BUTTON_PIN, INPUT);
  
  disp.setBrightness(7);
  disp.clear();

  // Start rgbMatrix object
  rgbMatrix.begin();
  rgbMatrix.setBrightness(10);

  for (int i = 0; i < rgbMatrixPixelsCount; ++i) {
      rgbMatrix.setPixelColor(i, offColor);
  }
  
  rgbMatrix.show();

  lastTime = millis();
}

int bombCounter = 59;

bool oldBigButton = false;

long unsigned int maxTime=0, minTime=-1, pastTime=0;

void loop() {
  pastTime=millis();
  bool disarmed = false;

  bool pin1 = digitalRead(DIP1_PIN);
  bool pin2 = digitalRead(DIP2_PIN);
  bool pin3 = digitalRead(DIP3_PIN);
  bool pin4 = digitalRead(DIP4_PIN);
  uint8_t code = (pin1<<3 | pin2<<2 | pin3<<1 | pin4<<0);

  bool bigButton = digitalRead(BIG_BUTTON_PIN);

  if (bigButton && oldBigButton != bigButton) {
    helperEnable = !helperEnable;
  }
  oldBigButton = bigButton;

  codeHelper(helperEnable, disarmCode, code);
      

  if (disarmCode == code) {
    disarmed = true;
  }

  if (!disarmed) {
    currTime = millis();
    if (currTime - lastTime > SECOND) {
      //Serial.print("disarmCode=");
      //Serial.print(disarmCode);
      //Serial.print(" Code=");
      //Serial.println(code);
      lastTime = currTime;
      bombCounter--;
      if (bombCounter < 0) {
        bombCounter = 0;
      }
    }
  }

  disp.showNumberDec(bombCounter, true);


   // beep
  beep(!disarmed, bombCounter);


/*
  disp.clear();
  disp.showNumberDec(-123, false);
  delay(TEST_DELAY);
  disp.showNumberHexEx(0xf1af);
  delay(TEST_DELAY);

  // !!!!! Our board have only column shown dots mask = 0x40
  // Run through all the dots
  for (k = 0; k <= 4; k++) {
    disp.showNumberDecEx(0, (0x80 >> k), true);
    delay(TEST_DELAY);
  }

  // !!!!! Our board have only column shown dots mask = 0x40
  // Run through all the dots
  for (k = 0; k <= 4; k++) {
    disp.showNumberDecEx(0, (0x80 >> k), true);
    delay(TEST_DELAY);
  }

  // Brightness Test
  for (k = 0; k < 7; k++) {
    disp.setBrightness(k);
    disp.showNumberHexEx(0xf1af);
    delay(TEST_DELAY);
  }

  // On/Off test
  for (k = 0; k < 4; k++) {
//    disp.setBrightness(7, false);  // Turn off
    disp.clear();
    delay(TEST_DELAY);
    disp.setBrightness(7, true); // Turn on
    disp.showNumberHexEx(0xf1af);
    delay(TEST_DELAY);
  }

  //                     0,     1,    2,    3
  uint8_t digits[4] = {0x3f, 0x06, 0x5b, 0x4f}; 
  
  for (int i = 0; i < 4; i++) {
    disp.clear();
    disp.setSegments(digits, 4, i);
    delay(TEST_DELAY*6);
  }
  */
  pastTime = millis()-pastTime;
  if(pastTime>maxTime)maxTime=pastTime;
    Serial.print("The MAX is:");
    Serial.println(maxTime);
  if(pastTime<minTime)minTime=pastTime;
  Serial.print("The MIN is:");
  Serial.println(minTime);
}

void beep(bool on, int beepSpeed)
{
  if (!on) {
    digitalWrite(BLUE_LED_PIN, false);
    digitalWrite(RED_LED_PIN, true);

    digitalWrite(BUZZER_PIN, false);
    return;
  }

  if (beepSpeed != 0) {
    digitalWrite(BLUE_LED_PIN, false);
    digitalWrite(BUZZER_PIN, false);
  }
  
  if (beepSpeed > 50) {
    delay(500);
  } else if (beepSpeed > 40) {
    delay(300);
  } else if (beepSpeed > 30) {
    delay(200);
  } else if (beepSpeed > 20) {
    delay(125);
  } else if (beepSpeed > 10) {
    delay(100);
  } else {
    delay(75);
  }
  
  digitalWrite(BLUE_LED_PIN, true);
  digitalWrite(RED_LED_PIN, false);
  digitalWrite(BUZZER_PIN, true);
}

void codeHelper(bool enabled, int matchCode, int currCode)
{
  const int pinsCount = 4;
  int result = matchCode^currCode;

  for (int i = 0; i < pinsCount; ++i) {
      bool miss = result & (1 << i);
      uint32_t currColor = miss ? missColor : matchColor;
      if (!enabled) {
        currColor = offColor;
      }
      rgbMatrix.setPixelColor(2*i, currColor);
      rgbMatrix.setPixelColor(2*i+1, currColor);
  }
  
  rgbMatrix.show();
}
