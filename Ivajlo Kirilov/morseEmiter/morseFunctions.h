void dot(OutputType output) {
  digitalWrite(output , HIGH);
  delay(TIME_UNIT);
  digitalWrite(output, LOW);
  delay(TIME_UNIT);
}

void dash(OutputType output) {
  digitalWrite(output , HIGH);
  delay(TIME_UNIT*3);
  digitalWrite(output, LOW);
  delay(TIME_UNIT);
}
void morse_space(){
  delay(4*TIME_UNIT);
}

void morse_A(OutputType output){
  dot(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_B(OutputType output) {
  dash(output);
  dot(output);
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_C(OutputType output) {
  dash(output);
  dot(output);
  dash(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_D(OutputType output) {
  dash(output);
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_E(OutputType output) {
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_F(OutputType output) {
  dot(output);
  dot(output);
  dash(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_G(OutputType output) {
  dash(output);
  dash(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_H(OutputType output) {
  dot(output);
  dot(output);
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_I(OutputType output) {
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_J(OutputType output) {
  dot(output);
  dash(output);
  dash(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_K(OutputType output) {
  dash(output);
  dot(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_L(OutputType output) {
  dot(output);
  dash(output);
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_M(OutputType output) {
  dash(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_N(OutputType output) {
  dash(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_O(OutputType output) {
  dash(output);
  dash(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_P(OutputType output) {
  dot(output);
  dash(output);
  dash(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_Q(OutputType output) {
  dash(output);
  dash(output);
  dot(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_R(OutputType output) {
  dot(output);
  dash(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_S(OutputType output) {
  dot(output);
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_T(OutputType output) {
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_U(OutputType output) {
  dot(output);
  dot(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_V(OutputType output) {
  dot(output);
  dot(output);
  dot(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_W(OutputType output) {
  dot(output);
  dash(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_X(OutputType output) {
  dash(output);
  dot(output);
  dot(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_Y(OutputType output) {
  dash(output);
  dot(output);
  dash(output);
  dash(output);
  delay(TIME_UNIT*2);
}

void morse_Z(OutputType output) {
  dash(output);
  dash(output);
  dot(output);
  dot(output);
  delay(TIME_UNIT*2);
}

void morse_function(char letter, OutputType output) {
  switch (letter) {
    case ' ':
      morse_space();
      break;
    case 'A':
      morse_A(output);
      break;
    case 'B':
      morse_B(output);
      break;
    case 'C':
      morse_C(output);
      break;
    case 'D':
      morse_D(output);
      break;
    case 'E':
      morse_E(output);
      break;
    case 'F':
      morse_F(output);
      break;
    case 'G':
      morse_G(output);
      break;
    case 'H':
      morse_H(output);
      break;
    case 'I':
      morse_I(output);
      break;
    case 'J':
      morse_J(output);
      break;
    case 'K':
      morse_K(output);
      break;
    case 'L':
      morse_L(output);
      break;
    case 'M':
      morse_M(output);
      break;
    case 'N':
      morse_N(output);
      break;
    case 'O':
      morse_O(output);
      break;
    case 'P':
      morse_P(output);
      break;
    case 'Q':
      morse_Q(output);
      break;
    case 'R':
      morse_R(output);
      break;
    case 'S':
      morse_S(output);
      break;
    case 'T':
      morse_T(output);
      break;
    case 'U':
      morse_U(output);
      break;
    case 'V':
      morse_V(output);
      break;
    case 'W':
      morse_W(output);
      break;
    case 'X':
      morse_X(output);
      break;
    case 'Y':
      morse_Y(output);
      break;
    case 'Z':
      morse_Z(output);
      break;
    default:
      morse_space();
      break;
  }
}
