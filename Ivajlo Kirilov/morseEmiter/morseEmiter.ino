#include"constants.h"
#include"enums.h"
#include"morseFunctions.h"

void setup() {
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop() {
  while( Serial.available() > 0 ){
    char curr = Serial.read();
    morse_function(curr , BUZZER);
  }
}
