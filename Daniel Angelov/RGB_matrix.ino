#include <Adafruit_NeoPixel.h>
#define rgbMatrixPin A3
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(64, rgbMatrixPin, NEO_GRB + NEO_KHZ800);
void setup() {
  rgbMatrix.begin();
  rgbMatrix.setBrightness(30);

}

void loop() {
  for(int i = 0;i < 64;i++){
    rgbMatrix.setPixelColor(i,4*i,255-4*i,0);
    rgbMatrix.show();
    
    }
  
}
