#include <Adafruit_NeoPixel.h>
#define rgbMatrixPin A0
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(64, rgbMatrixPin, NEO_GRB + NEO_KHZ800);
//#define VRxPIN A3
#define VRyPIN A1
//#define ButtonPIN A7 //не е свързан

int ball = 0;
int xball = 3;
int yball = 3;
int xsped = 1;
int ysped = 0;
int board = 57;
int xbrd = 7;
int ybrd = 1;
int blank = 0;
int start = 0;
int color = 0;
int cross[16] = {0, 9, 18, 27, 36, 45, 54, 63, 56, 49, 42, 35, 28, 21, 14, 7};

void setup() {
  Serial.begin(9600);

  rgbMatrix.begin();
  rgbMatrix.setBrightness(5);

  //board
  //pinMode(VRxPIN, INPUT);
  pinMode(VRyPIN, INPUT);
  //pinMode(ButtonPIN, INPUT_PULLUP);
  rgbMatrix.setPixelColor(board, 255, 255, 255);
  rgbMatrix.setPixelColor(board + 1, 255, 255, 255);
  rgbMatrix.setPixelColor(board - 1, 255, 255, 255);
  rgbMatrix.show();
}
int blankball = 0;
void loop() {
  int yreverse = analogRead(VRyPIN);
  const int koloni = 8;

  if (yreverse < 400) {
    ybrd  = ybrd + 1;
  }
  else if (500 < yreverse) {
    ybrd  = ybrd - 1;
  }
  if (ybrd > 6) {
    ybrd = ybrd - 1;
  }
  if (ybrd < 1) {
    ybrd = ybrd + 1;
  }
  board  = xbrd * koloni + ybrd;
  Serial.println(xball);
  if (xball < 7) {
    color = 255;
  }
  else if (xball > 7) {
    color = 0;
    for(int i = 0;i<16;i++){
    rgbMatrix.setPixelColor(cross[i], 255, 0, 0);
    }
    rgbMatrix.show();
    if (ybrd > 5) {
    ybrd = ybrd - 1;
    }
    if (ybrd < 2) {
    ybrd = ybrd + 1;
    }
  }
  rgbMatrix.setPixelColor(board, color, color, color);
  rgbMatrix.setPixelColor(board + 1, color, color, color);
  rgbMatrix.setPixelColor(board - 1, color, color, color);
  if (board < blank) {
    rgbMatrix.setPixelColor(blank + 1, 0, 0, 0);
    blank = board;
  }
  if (board > blank) {
    rgbMatrix.setPixelColor(blank - 1, 0, 0, 0);
    blank = board;
  }

  delay(300);

  //ball



  if (ybrd > 1) {
    start = 1;
  }
  if (start == 1) {
    if (ball == board - 9) {
      ysped = - 1;
      xsped = -1;
    }
    if (ball == board - 8) {
      ysped = 0;
      xsped = -1;
    }
    if (ball == board - 7) {
      ysped = 1;
      xsped = -1;
    }
    switch (yball) {
      case 0:
        ysped = 1;
        break;
      case 7:
        ysped = -1;
        break;
    }
    if (xball == 0) {
      xsped = 1;
    }


  }

  yball = yball + ysped;
  xball = xball + xsped;
  ball = 8 * xball + yball;

  rgbMatrix.setPixelColor(ball, 255, 0, 0);
  if (ball != blankball) {
    rgbMatrix.setPixelColor(blankball, 0, 0, 0);
    blankball = ball;
  }
  rgbMatrix.show();

}
