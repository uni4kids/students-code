#include <TM1637Display.h>
#include "mega2560_pinout.h"
TM1637Display display(SEGMENT_DISPLAY_CLK_PIN, SEGMENT_DISPLAY_DIO_PIN);
unsigned sec = 0;
unsigned min = 0;
unsigned long clock = 0;
unsigned long passed_time = 0;
void setup() {
  Serial.begin(9600);
  //Serial.print(sizeof(unsigned long));
  display.setBrightness(7);
  display.clear();
  //display.showNumberDec(sec, 1);
  //display.showNumberDec(min, 1, 2, 1);
}

void loop() {
  
  if (passed_time >= 1000) {
  Serial.print(sec);
  Serial.print(" ");
  Serial.print(min);
  Serial.print(" ");
  Serial.print(clock);
  Serial.print(" ");
  Serial.println(passed_time);
    clock = min + sec;
    display.showNumberDecEx(clock, 0b11100000, 1);
    sec = sec + passed_time / 1000;
    if (sec == 60) {
      sec = 0;
      min = min + 100;
    }
  }
  passed_time = millis() - sec * 1000 - min*600;
  
}
