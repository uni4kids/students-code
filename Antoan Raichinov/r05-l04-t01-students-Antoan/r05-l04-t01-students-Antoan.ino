#include "mega2560_pinout.h"
#include "VirtuinoBluetooth.h"
VirtuinoBluetooth virtuino(Serial1);

void setup()
{
  pinMode(BLUE_LED_PIN, OUTPUT);
  Serial1.begin(9600);
}

void loop()
{
  virtuino.run();
  if (virtuino.vMemoryRead(1) == 1)
  {
    digitalWrite(BLUE_LED_PIN, HIGH);
  }
  else digitalWrite(BLUE_LED_PIN, LOW);
}
