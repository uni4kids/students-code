#define LED_RED (13)
#define button_PIN (12)
void setup() {
pinMode(LED_RED, OUTPUT);
pinMode(button_PIN, INPUT);
}

void loop() {
bool isbuttonPressed;
isbuttonPressed=digitalRead(button_PIN);

if (isbuttonPressed)

 {
   digitalWrite(LED_RED, HIGH);
 }
 else
 {
   digitalWrite(LED_RED, LOW);
 }
}
