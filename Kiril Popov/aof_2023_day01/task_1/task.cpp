#include <sstream>
#include <fstream>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <cctype>

using namespace std;

int parseChars(string str) {
    // Complete this function
    int number = 0;

    for (int i = 0; i < str.size(); ++i) {
        if (isdigit(str.at(i))) {
            number = (str.at(i) -'0')*10;
            break;
        }
    }

    for (int i = str.size() - 1; i > -1; --i) {
        if (isdigit(str.at(i))) {
            number += (str.at(i) -'0');
            break;
        }
    }

    return number;
}

int main (void)
{
    cout << "Program Start" << endl;
    ifstream myfile;
    myfile.open("input.txt");
    vector<int> codes;
    long int sum = 0;
    int lineNum = 0;

    if (myfile.is_open()) {
        string line;
        while (getline(myfile, line)) {
            // if (line.size() > 0) {
            //     // cout << "Line Size: " << line.size() << endl;
            //     cout << line << endl;
            // } else {
            //     cout << "Line Size: 0" << endl;
            //     // cout << line << endl;
            // }
            int tmpNumber = parseChars(line);

            sum += tmpNumber;
            lineNum++;
            cout << lineNum <<"Num: " << tmpNumber << endl;
            cout << "Sum: " << sum << endl;
        }
        myfile.close();
    } else {
        cout << "File Open: failed" << endl;
    }

    cout << "Final sum: " << sum << endl;
	cout << "Program end" << endl;

    return 0;
}