#define buzzerPin 6

void dot()
{
  tone(buzzerPin, 1000, 100);
  delay(200);
}

void dash()
{
  tone(buzzerPin, 1000, 300);
  delay(400);
}

void setup() {
  pinMode(buzzerPin, OUTPUT);

  dot();
}
 dash();

void loop() {
}