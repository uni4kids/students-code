#include "mega2560_pinout.h"
#define btn1 DP02 
#define btn2 DP03 
#define btn3 DP04 
#define btn4 DP05
#define led1 DP06  
#define led2 DP07  
#define led3 DP08  
#define led4 DP09  


void setup() {
  pinMode(btn1, INPUT_PULLUP);
  pinMode(btn2, INPUT_PULLUP);
  pinMode(btn3, INPUT_PULLUP);
  pinMode(btn4, INPUT_PULLUP);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  Serial.begin(9600);

  digitalWrite(led1, HIGH);
  delay(1000);
  digitalWrite(led1, LOW);
  delay(100);

  digitalWrite(led2, HIGH);
  delay(1000);
  digitalWrite(led2, LOW);
  delay(100);

  digitalWrite(led3, HIGH);
  delay(1000);
  digitalWrite(led3, LOW);
  delay(100);

  digitalWrite(led4, HIGH);
  delay(1000);
  digitalWrite(led4, LOW);
  delay(100);
}

void loop() {
  bool btn1state = digitalRead(btn1);
  bool btn2state = digitalRead(btn2);
  bool btn3state = digitalRead(btn3);
  bool btn4state = digitalRead(btn4);

  Serial.print(btn1state);
  Serial.print(btn2state);
  Serial.print(btn3state);
  Serial.println(btn4state);

  delay(100);
}
