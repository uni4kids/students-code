#include "mega2560_pinout.h" 

int dp[4] = {DP06, DP07, DP08, DP09};

void setup() {
  pinMode(DP02, INPUT_PULLUP);
  pinMode(DP03, INPUT_PULLUP);
  pinMode(DP04, INPUT_PULLUP);
  pinMode(DP05, INPUT_PULLUP);
  
  pinMode(DP06, OUTPUT);
  pinMode(DP07, OUTPUT);
  pinMode(DP08, OUTPUT);
  pinMode(DP09, OUTPUT);
  Serial.begin(9600);

  reset();

  for(int i = 0; i < 4; i++){
    digitalWrite(dp[i], HIGH);
    Serial.print("Diode ");
    Serial.print(i);
    Serial.println(": HIGH");
    delay(2000);
    reset();
    Serial.print("Diode ");
    Serial.print(i);
    Serial.println(": LOW");
  }
}

void loop() {
  Serial.println(digitalRead(DP02));
  Serial.println(digitalRead(DP03));
  Serial.println(digitalRead(DP04));
  Serial.println(digitalRead(DP05));
  delay(1000);
}

void reset(){
  digitalWrite(DP06, LOW);
  digitalWrite(DP07, LOW);
  digitalWrite(DP08, LOW);
  digitalWrite(DP09, LOW);
}
