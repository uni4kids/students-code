#include "mega2560_pinout.h"
#define BUTTON1PIN BIG_BUTTON_PIN
#define BUTTON2PIN DP03
#define BUTTON3PIN DP04

int current_time = 0;
int begin_time = -1000;

enum class choices{undefined = -1, rock = 0, paper = 1, scissors = 2};

choices choice = choices::undefined;

struct buttons {
  bool btn1;
  bool btn1_last;
  bool btn2;
  bool btn2_last;
  bool btn3;
  bool btn3_last;
};

buttons buttonState = {false, false, false, false, false, false};

choices converter(struct buttons buttonState) {
  if (buttonState.btn1 && !buttonState.btn2 && !buttonState.btn3) {
    return choices::rock;
  }
  if (!buttonState.btn1 && buttonState.btn2 && !buttonState.btn3) {
    return choices::paper;
  }
  if (!buttonState.btn1 && !buttonState.btn2 && buttonState.btn3) {
    return choices::scissors;
  }
  return choices::undefined;
}

void readPins(){
  buttonState.btn1 = digitalRead(BUTTON1PIN);
  buttonState.btn2 = digitalRead(BUTTON2PIN);
  buttonState.btn3 = digitalRead(BUTTON3PIN);

  if(buttonState.btn1 != buttonState.btn1_last && buttonState.btn1 == true){
    choice = choices::rock;
    begin_time = millis();
  }
  if(buttonState.btn2 != buttonState.btn2_last && buttonState.btn2 == true){
    choice = choices::paper;
    begin_time = millis();
  }
  if(buttonState.btn3 != buttonState.btn3_last && buttonState.btn2 == true){
    choice = choices::scissors;
    begin_time = millis();
  }
  buttonState.btn1_last = buttonState.btn1;
  buttonState.btn2_last = buttonState.btn2;
  buttonState.btn3_last = buttonState.btn3;
}

void setup() {
  Serial.begin(9600);
  pinMode(BUTTON1PIN, INPUT_PULLUP); //Rock
  pinMode(BUTTON2PIN, INPUT_PULLUP); //Paper
  pinMode(BUTTON3PIN, INPUT_PULLUP); //Scissors
}

void loop() {
  current_time = millis() - begin_time;
  if(current_time >= 200){
    readPins();
  }
  if(choice != choices::undefined){
    Serial.print((int)choice);
    Serial.print(" -> ");
    Serial.println(millis());
    choice = choices::undefined;
  }
}
