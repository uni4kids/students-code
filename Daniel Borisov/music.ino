#define BUZZ 6
int musuc = 100;
void setup() {
  pinMode(BUZZ, OUTPUT);
}

void loop() {
  tone(BUZZ, 988, musuc);
  delay(500);
  tone(BUZZ, 65, musuc);
  delay(500);
  tone(BUZZ, 1175, musuc);
  delay(500);
  tone(BUZZ, 82, musuc);
  delay(500);
  tone(BUZZ, 1319, musuc);
  delay(500);
  tone(BUZZ, 98, musuc);
  delay(500);
  tone(BUZZ, 1568, musuc);
  delay(500);
  tone(BUZZ, 123, musuc);
  delay(500);
}