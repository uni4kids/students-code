#include <Adafruit_NeoPixel.h>
#include "VirtuinoBluetooth.h"
#include "mega2560_pinout.h"

int x = 0;
int y = 0;
bool left = 0;
bool right = 0;
float point = 0.5f;
int brightness = 255;

VirtuinoBluetooth virtuino(Serial1);
Adafruit_NeoPixel pixel(8, RGB_LED_MATRIX_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(JOYSTICK_X_PIN, INPUT);
  pinMode(JOYSTICK_Y_PIN, INPUT);
  pixel.begin();
  pixel.show();
}

void loop() {
  virtuino.run();
  for(int i = 0; i < 8; i++){
    pixel.setPixelColor(i, 0, 0, 0);
  }
  x = analogRead(JOYSTICK_X_PIN);
  y = analogRead(JOYSTICK_Y_PIN);
  if(x >= 0 && x < 400){
    left = 1;
  }else{
    left = 0;
  }
  if(x > 600 && x <= 1023){
    right = 1;
  }else{
    right = 0;
  }
  if(left == 1 && right == 0 && point > 0.0f){
    point -= 0.005f;
  }
  if(right == 1 && left == 0 && point < 1.0f){
    point += 0.005f;
  }
  brightness = virtuino.vMemoryRead(0);
  pixel.setPixelColor((int)((1.0f - point) * 7), brightness, brightness, brightness);
  pixel.show();
}
