#include "mega2560_pinout.h"

// Initialize all needed pins and libraries
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial2.begin(9600);

  pinMode(SET_HC12_PIN, OUTPUT);
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(DIP1_PIN, INPUT);
}

const int commandsize = 2;
char commandStart = 'v';
char commandOn = '1';
char commandOff = '0';

char myChar;
char string[commandsize]; //= malloc(10 * sizeof(char));
int i = 0;
const char *commandon = "v1";
const char *commandoff = "v0";

// Write the code executed in a loop
void loop() {
  bool pin1 = digitalRead(DIP1_PIN);

  if (pin1 == HIGH) {
    digitalWrite(SET_HC12_PIN, HIGH);
  } else {
    digitalWrite(SET_HC12_PIN, LOW);
  }

  if (Serial.available()) {
    Serial2.write(Serial.read());
  }

  if(Serial2.available()){
    string[i] = Serial2.read();
    Serial.write(string[i]);
    i++;
  }

  if(i == commandsize){
    string[i] = '\0';
    if(strcmp(string, commandon) == 0){
      digitalWrite(RED_LED_PIN, HIGH);
      i = 0;
    }else if(strcmp(string, commandoff) == 0){
      digitalWrite(RED_LED_PIN, LOW);
      i = 0;
    }else{
      i = 0;
    }
  }else if(i < commandsize){
    if(string[i - 1] != 'v'){
      i = 0;
    }
  }
}