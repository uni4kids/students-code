#include "mega2560_pinout.h"

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial2.begin(9600);
  pinMode(SET_HC12_PIN, OUTPUT);
  pinMode(TOUCH_PIN, INPUT);
  //pinMode(DIP1_PIN, INPUT);
}

void loop() {
  if(digitalRead(DIP1_PIN) == LOW){
    digitalWrite(SET_HC12_PIN, HIGH);
  }else{
    digitalWrite(SET_HC12_PIN, LOW);
  }
  if(Serial.available()){
    Serial2.write(Serial.read());
  }
  if(Serial2.available()){
    Serial.write(Serial2.read());
  }
}
