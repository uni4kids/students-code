#include <SoftwareSerial.h>
#include <VirtuinoBluetooth.h>

#define rx 2
#define tx 3

#define led 13

#define M1A 11
#define M1B 5  //дефинираме пиновете на моторите
#define M2A 9  //дефинираме пиновете на моторите
#define M2B 10

SoftwareSerial rusaa(tx, rx);
VirtuinoBluetooth virtuino(rusaa);

bool state = 0;
bool locked = 0;
bool temp = 0;
bool reversestate = 1;
bool front = 0;
bool back = 0;
bool lside = 0;
bool rside = 0;
int speed = 255;
unsigned long long timer = 0;

void setup() {
  // put your setup code here, to run once:
  rusaa.begin(9600);
  pinMode(led, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  virtuino.run();
  locked = virtuino.vMemoryRead(2);
  /*
  if(state == 1){
    digitalWrite(led, HIGH);
  }else{
    digitalWrite(led, LOW);
  }
  */
  front = virtuino.vMemoryRead(3);
  back = virtuino.vMemoryRead(4);
  lside = virtuino.vMemoryRead(5);
  rside = virtuino.vMemoryRead(6);
  speed = virtuino.vMemoryRead(7);
  movementreset();
  if(front == 1 && back == 0 && lside == 0 && rside == 0 && locked == 0){
    forward();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(rside == 1 && front == 0 && back == 0 && lside == 0 && locked == 0){
    right();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(back == 1 && front == 0 && lside == 0 && rside == 0 && locked == 0){
    backward();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(lside == 1 && front == 0 && back == 0 && rside == 0 && locked == 0){
    left();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(front == 1 && lside == 1 && rside == 0 && back == 0 && locked == 0){
    left();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(front == 1 && rside == 1 && lside == 0 && back == 0 && locked == 0){
    right();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(back == 1 && lside == 1 && rside == 0 && front == 0 && locked == 0){
    backleft();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(back == 1 && rside == 1 && lside == 0 && front == 0 && locked == 0){
    backright();
    digitalWrite(led, LOW);
    timer = 0;
  }
  if((front == 1 || back == 1 || lside == 1 || rside == 1) && locked == 1){
    digitalWrite(led, LOW);
    timer = 0;
  }
  if(front == 0 && back == 0 && lside == 0 && rside == 0){
    if(state == 1){
      digitalWrite(led, HIGH);
    }else{
      digitalWrite(led, LOW);
    }
  }
  if(timer % 1000 == 0){
    temp = state;
    state = reversestate;
    reversestate = temp;
  }
  timer++;
}

void forward() {
  /*
  digitalWrite(M1A, HIGH);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, HIGH);
  digitalWrite(M2B, LOW);
  */
  analogWrite(M1A, speed);
  analogWrite(M1B, 0);
  analogWrite(M2A, speed);
  analogWrite(M2B, 0);
}

void backward() {
  /*
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, HIGH);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, HIGH);
  */
  analogWrite(M1A, 0);
  analogWrite(M1B, speed);
  analogWrite(M2A, 0);
  analogWrite(M2B, speed);
}

void left() {
  /*
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, HIGH);
  digitalWrite(M2B, LOW);
  */
  analogWrite(M1A, 0);
  analogWrite(M1B, 0);
  analogWrite(M2A, speed);
  analogWrite(M2B, 0);
}

void right() {
  /*
  digitalWrite(M1A, HIGH);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
  */
  analogWrite(M1A, speed);
  analogWrite(M1B, 0);
  analogWrite(M2A, 0);
  analogWrite(M2B, 0);
}

void backleft(){
  /*
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, HIGH);
  */
  analogWrite(M1A, 0);
  analogWrite(M1B, 0);
  analogWrite(M2A, 0);
  analogWrite(M2B, speed);  
}

void backright(){
  /*
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, HIGH);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
  */
  analogWrite(M1A, 0);
  analogWrite(M1B, speed);
  analogWrite(M2A, 0);
  analogWrite(M2B, 0);  
}

void movementreset() {
  /*
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
  */
  analogWrite(M1A, 0);
  analogWrite(M1B, 0);
  analogWrite(M2A, 0);
  analogWrite(M2B, 0);
}

//$cmd:1@
//String cmd = "cmd";
//int val = 1;