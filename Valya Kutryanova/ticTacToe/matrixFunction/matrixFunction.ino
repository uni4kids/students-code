#define num 7
#define mtrxPin 0

const char p1 = '1';
const char p2 = '2';

#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel matrix(64, mtrxPin, NEO_GBR + NEO_KHZ800);

void setup() {
  pinMode(mtrxPin, OUTPUT);
  matrix.begin();
  matrix.setBrightness(70);

}

void loop() {
  

}

void drawMatrix(row, col, player){
  mtrxRow = row * 3;
  mtrxCol = col * 3;
  if (player == p1){
    matrix.setPixelColor((num * mtrxRol + mtrxCol), 0, 0, 255);
    matrix.setPixelColor((num * mtrxRol + mtrxCol + 1), 0, 0, 255);
    matrix.setPixelColor((num * (mtrxRol + 1) + mtrxCol), 0, 0, 255);
    matrix.setPixelColor((num * (mtrxRol + 1) + mtrxCol + 1), 0, 0, 255);    
  }
  else if (player == p2){
    matrix.setPixelColor((num * mtrxRol + mtrxCol), 255, 0, 0);
    matrix.setPixelColor((num * mtrxRol + mtrxCol + 1), 255, 0, 0);
    matrix.setPixelColor((num * (mtrxRol + 1) + mtrxCol), 255, 0, 0);
    matrix.setPixelColor((num * (mtrxRol + 1) + mtrxCol + 1), 255, 0, 0);
  }
}

void matrix(int BoardState[3][3]){
  for (int i = 0; i < 4; i++){
    for (int j = 0; i < 4; i++){
      drawMatrix(i, j, BoardState[i][j]);
    }
  }
}