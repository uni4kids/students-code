#include <Adafruit_NeoPixel.h>
#include "mega2560_pinout.h"

#define RGB_Matrix_PIN A2

const int rgbMatrixPixelsCount = 64;

Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(rgbMatrixPixelsCount, RGB_Matrix_PIN, NEO_GRB + NEO_KHZ800);


//logic

const int rows = 8;
const int colums = 8;
const int colors = 3;

int pixels[rows][colums][colors] = { 0 };

void setup() {
  rgbMatrix.begin();
  rgbMatrix.setBrightness(10);

  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < colums; j++) {
      for (int k = 0; k < colors; k++) {
        if (k == 0) {
          pixels[i][j][k] = 255;
        }
        else{
          pixels[i][j][k] = j * 8 + i * 8;
        }
      }
    }
  }
}

void loop() {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < colums; j++) {
      rgbMatrix.setPixelColor(j + i * rows, pixels[i][j][0], pixels[i][j][1], pixels[i][j][2]);
    }
  }


  rgbMatrix.show();
  delay(200);
}
